package main

import (
	"fmt"
	"math/rand"

	"golang.org/x/tour/pic"
)

/**
I only accept slices as arguments... no arrays.
I I had to accept arrays, then I'd have to restrict its size for ever!
*/
func playWithSlices(numbers []int) {

	fmt.Println("Slice lenght: ", len(numbers), ", capacity: ", cap(numbers))
	fmt.Println(numbers)

	// now extending the slice lenght
	numbers = numbers[:10]
	fmt.Println("Slice lenght: ", len(numbers), ", capacity: ", cap(numbers))
	fmt.Println(numbers)

	// now reducing the slice lenght
	numbers = numbers[2:7]
	fmt.Println("Slice lenght: ", len(numbers), ", capacity: ", cap(numbers))
	fmt.Println(numbers)

	var nilSlice []int
	fmt.Println(nilSlice == nil)
}

func makeSlices() {
	s1 := make([]int, 5)
	s2 := make([]int, 0, 10)

	printSlice(s1)
	printSlice(s2)
}

func printSlice(s []int) {
	fmt.Println("len:", len(s), " cap:", cap(s), "slice:", s)
}

func printStringSlice(s []string) {
	fmt.Println("len:", len(s), " cap:", cap(s), "slice:", s)
}

func playWithMultiDimSlices() {
	matrix := [][]string{
		[]string{"_", "_", "_"},
		[]string{"-", "-", "-"},
		[]string{"x", "x", "x"},
	}

	for i := 0; i < len(matrix); i++ {
		fmt.Println(matrix[i])
	}

}

func appendingSlices() {
	myArray := [5]string{
		"one", "two", "three", "four", "five",
	}

	mySlice := myArray[0:4] // slicing 1 element shorter
	mySlice[0] = "eins"

	printStringSlice(myArray[:])
	printStringSlice(mySlice)

	// appending 1 element which still fits in the array on the back
	myAppendedSlice := append(mySlice, "fünf")
	myAppendedSlice[1] = "zwei" // affecting also the original array

	printStringSlice(myArray[:])
	printStringSlice(mySlice)
	printStringSlice(myAppendedSlice)

	// now appending a further element which overflows the original array
	// so on the backgournd a new array is created to have enough space
	// for the slice
	myAppendedSlice = append(myAppendedSlice, "sechs")
	// now affecting the new array only
	myAppendedSlice[2] = "drei"

	printStringSlice(myArray[:])
	printStringSlice(mySlice)
	printStringSlice(myAppendedSlice) // also note the capacity change

}

func rangingSlices() {

	fibonacci := [4]int{1, 2, 3, 5}
	fs := fibonacci[:]

	// ranging index + value
	for i, v := range fs {
		fmt.Println("i:", i, " v", v)
	}

	// ranging index
	for i := range fs {
		fmt.Println("i:", i)
	}

	// ranging value
	for _, v := range fs {
		fmt.Println(" v", v)
	}
}

func Pic(dx, dy int) [][]uint8 {
	var pic = make([][]uint8, dx)

	for x := 0; x < dx; x++ {
		pic[x] = make([]uint8, dy)
		for y := 0; y < dy; y++ {
			//pic[x][y] = uint8((x + y) / 2)
			pic[x][y] = uint8(x * y)
			//pic[x][y] = uint8(x ^ y)
		}
	}
	return pic
}

func main() {
	var a [2]string
	a[0] = "hello"
	a[1] = " world"
	fmt.Println(a[0], a[1])

	var numbers [10]int
	for i := 0; i < 10; i++ {
		numbers[i] = rand.Int()
	}
	fmt.Println(numbers)

	// let's do some slices
	s := numbers[0:4]
	fmt.Println(s)

	// now changing the array through the slice
	s2 := s[0:2]
	fmt.Println(s2)
	s2[0] = 666
	fmt.Println(numbers)

	// slice literals -> creates an array and then a slice pointing to it
	sl := []int{1, 2}
	fmt.Println(sl)

	// slice literal of a struct
	ssl := []struct {
		x, y int
	}{
		{1, 2},
		{3, 4},
	}
	fmt.Println(ssl)

	// default bounds by slices
	sb1 := numbers[0:10]
	sb2 := numbers[:10]
	sb3 := numbers[0:]
	sb4 := numbers[:]
	fmt.Println(sb1, sb2, sb3, sb4)

	playWithSlices(numbers[:5])
	makeSlices()
	playWithMultiDimSlices()
	appendingSlices()
	rangingSlices()
	pic.Show(Pic)

}
