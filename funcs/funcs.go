package main

import (
	"fmt"
	"math"
)

func callAnotherFunc(fn func(float64, float64) float64) float64 {
	return fn(2, 3) + 1
}

// let's fuck our brains with closures
func adder() func(int) int {
	var sum = 0
	return func(x int) int {
		sum += x
		return sum
	}
}

func fibonacci() func() int {
	var f0 = 0
	var f1 = 1
	var fn = f0 + f1
	var n = 0

	return func() int {
		var ret = 0
		switch n {
		case 0:
			ret = f0
		case 1:
			ret = f1
		default:
			ret = fn

			// let's recalculate the serie
			f0 = f1
			f1 = fn
			fn = f0 + f1
		}
		n++
		return ret
	}
}

func main() {
	hypot := func(x, y float64) float64 {
		return math.Sqrt(x*x + y*y)
	}

	fmt.Println(hypot(3, 3))

	fmt.Println(callAnotherFunc(hypot))
	fmt.Println(callAnotherFunc(math.Pow))

	// using the closure
	// have in mind that this will create 2 instances of the adder function
	// each of them with its own sum variable, which will be updated
	// independently across calls to pos() or neg()
	pos, neg := adder(), adder()
	for i := 0; i < 10; i++ {
		fmt.Println(
			pos(i),
			neg(-2*i),
		)
		//	fmt.Println("neg:", neg(-2*i))
	}

	fib := fibonacci()
	for i := 0; i < 50; i++ {
		fmt.Println(fib())
	}

}
