package main

import (
	"fmt"
	"math"
)

type Vertex struct {
	X, Y float64
}

type MyFloat float64

// methods are just functions: func Abs(v Vertex) float64 {...}
func (v Vertex) Abs() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

// just to excemplify abs using also a pointer reciever,
// however all methos on a given type should have either pointer  or
// value recievers
func (v *Vertex) AbsPointerReceiver() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func (f MyFloat) Abs() float64 {
	if f < 0 {
		return float64(-f)
	}
	return float64(f)
}

/*
Method with pointer reciever
*/
func (v *Vertex) Scale(f float64) {
	// affecting the content of the Vertex
	v.X = v.X * f
	v.Y = v.Y * f
}

func main() {
	v := Vertex{3, 4}
	fmt.Println(v.Abs())

	f := MyFloat(float64(-2.3))
	// Using the reciever correctly
	fmt.Println(f.Abs())

	// Letting golang interpret it actually requires a MyFloat and not a pointer
	fmt.Println((&f).Abs())

	// Using just v as a reciever, letting golang interpret it as &v
	v.Scale(10)
	fmt.Println(v)

	// Using the reference explicitly as a pointer reciever
	(&v).Scale(10)
	fmt.Println(v)
}
