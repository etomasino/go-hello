package main

import (
	"fmt"
	"math"
	"runtime"
	"time"
)

func pow(x, n, lim float64) float64 {
	if v := math.Pow(x, n); x < lim {
		return v
	} else {
		// Accessing v from the else block
		fmt.Println("pow result overflows lim: ", v)
	}
	return lim
}

func Sqrt(x float64) float64 {
	var z float64 = x
	for i := 1; i < 10; i++ {
		z = z - (math.Pow(z, 2)-x)/(2*z)
		fmt.Println("z", i, ": ", z)
	}
	return z
}

func TellWhichSO() string {
	switch os := runtime.GOOS; os {
	case "darwin":
		return "OS X."
	case "linux":
		return "Linux AF"
		//fallthrough
	default:
		return os
	}
}

func HowLongToFriday() string {
	today := time.Now().Weekday()
	switch time.Friday {
	case today + 0:
		return "Today!"
	case today + 1:
		return "Tomorrow!"
	default:
		return "Far away"
	}
}

func GrussMal() string {
	t := time.Now()

	switch {
	case t.Hour() < 12:
		return "Guten Morgen!"
	case t.Hour() < 16:
		return "Guten Tag"
	case t.Hour() < 23:
		return "Guten Abend"
	default:
		return "Gute Nacht"
	}
}

func LetsDeferSomething() {
	defer fmt.Println("To be called later...")
	fmt.Println("Do something")
}

func StackSomeDefers() {
	for i := 0; i < 10; i++ {
		defer fmt.Println(i)
	}
	fmt.Println("End of defer stacking")
}

func main() {

	/*
		sum := 0
		for i := 0; i < 10; i++ {
			fmt.Println(i)
			sum++
		}
		fmt.Println(sum)

		// for as while
		for sum > 0 {
			sum--
			fmt.Println(sum)
		}
	*/

	// fmt.Println(pow(2, 2, 2))
	fmt.Println(Sqrt(4.0))
	fmt.Println("I'm running on: ", TellWhichSO())
	fmt.Println("How long until next Friday: ", HowLongToFriday())
	LetsDeferSomething()
	StackSomeDefers()

}
