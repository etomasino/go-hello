package main

import "fmt"
import "strings"
import "golang.org/x/tour/wc"

type Vertex struct {
	Lat, Long float64
}

var m map[string]Vertex //creating a nil map, entries can't be added still

//map literals
var litm = map[string]Vertex{
	"hell":   Vertex{66.6, 66.6},
	"heaven": Vertex{00.0, 00.0},
}

//simplified map literals
var simplitm = map[string]Vertex{
	"hell":   {66.6, 66.6},
	"heaven": {00.0, 00.0},
}

func mutateMap(paramMap map[string]Vertex) {
	fmt.Println("About to mutate:", paramMap)

	//retrieving an element
	//this returns a copy of the element, as in everything in go
	hellValue := paramMap["hell"]
	hellValue.Lat = 11.1
	hellValue.Long = 22.2
	fmt.Println("paramMap:", paramMap)

	// the following ain't valid
	// map elements are not addressable
	// since changes in the map may lead to changes in the addresses
	// hellValueP := &paramMap["hell"]

	//assinging the value again
	paramMap["hell"] = hellValue
	fmt.Println("paramMap:", paramMap)

	//assinging a value if not present
	//mind the map returning two values
	value1, hasIt := paramMap["earth"]
	if !hasIt {
		value1 = Vertex{33.3, 22.2}
		paramMap["earth"] = value1
	}

	fmt.Println("paramMap:", paramMap)

}

func WordCount(str string) map[string]int {
	wordCount := make(map[string]int)

	splittedString := strings.Split(str, " ")
	for i := 0; i < len(splittedString); i++ {
		v := wordCount[splittedString[i]]
		v++
		wordCount[splittedString[i]] = v
	}
	return wordCount
}

func main() {
	m = make(map[string]Vertex)
	m["myKey"] = Vertex{50.1, 50.2}

	fmt.Println("m:", m)
	fmt.Println("m[myKey]:", m["myKey"])
	fmt.Println("litm:", litm)
	fmt.Println("litm:", simplitm)

	mutateMap(simplitm)
	wordCount := WordCount("hello this is my hello word count count")
	fmt.Println("wordCount:", wordCount)
	wc.Test(WordCount)

}
