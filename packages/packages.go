package main

import (
	"fmt"
	"math"
	"math/rand"
)

var c, python, java = true, 1234, "NO!"
var i, j int = 1, 2

func sum(a, b int) int {
	return a + b
}

func swap(a int, b string) (string, int) {
	return b, a
}

func nakedReturn(in int) (out int) {
	out = in + 1
	return
}

func main() {
	fmt.Println("My favourite number is: ", rand.Intn(10))
	fmt.Println("Pi: ", math.Pi)
	fmt.Println("My sum", sum(2, 3))
	element, quantity := swap(1, "Car")
	fmt.Println("My swap is: ", element, quantity)
	fmt.Println("My nakedReturn: ", nakedReturn(1))

	var i int
	fmt.Println("My variables: ", i, j, c, python, java)

}
