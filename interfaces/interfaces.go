package main

import (
	"bytes"
	"fmt"
	"math"
	"strconv"
)

type Abser interface {
	Abs() float64
	// DoMore() int  // adding this extra method will cause the compilation to fail, since it's not being implemented
}

type I interface {
	M()
}

// describing the interface as (value, Type)
func describe(i I) {
	fmt.Printf("(%v, %T)\n", i, i)
}

func describeAnything(i interface{}) {
	fmt.Printf("(%v, %T)\n", i, i)
}

// Mind the type keyboard for switching by type.
// It's restricted to switch statements.
func switchByType(i interface{}) {
	// type switching...
	switch v := i.(type) {
	case int:
		//v.T will be int
		fmt.Printf("Twice %v is %v\n", v, v*2)
	case string:
		//v.T will be string
		fmt.Printf("%q is %v bytes long\n", v, len(v))
	default:
		//v.T will be whaterver i.T is
		fmt.Printf("I don't know what a heck is %T\n", v)
	}
}

func main() {
	fmt.Println("interfaces")

	// Playing with Abser
	var a Abser
	var mf = MyFloat(-math.Sqrt2)
	var v = Vertex{3, 4}

	// faster your seatbelt, let's do some polymorphism
	a = mf //this assignment is valid becuase there is an Abs() defined for MyFloat
	fmt.Println(a.Abs())

	a = &v //this is valid becuase there is an Abs() defined for *Vertex
	fmt.Println(a.Abs())

	// a = v //this won't be valid, since Abs() ain't defined for Vertex (only for its pointer)

	// Playing with I
	var i I

	describe(i) // interface value with no value or type -> nil
	// i.M() // uncommenting lead to runtime error, since the interface value is nil (doesn't have a type)

	i = &T{"Hello"}
	describe(i)
	i.M()

	i = F(math.Pi)
	describe(i)
	i.M()

	var t2 *T
	i = t2
	describe(i) //inteface value, with nil value but with type -> not nil itself
	i.M()       //uncommenting this line would lead to npe, unless handling on M()
	fmt.Println(i)

	// Using an empty interface to describe whatever we want

	var ei interface{}
	describeAnything(ei)

	ei = 42
	describeAnything(ei)
	switchByType(ei)

	ei = "message for me"
	describeAnything(ei)
	switchByType(ei)

	switchByType(true)
	switchByType(float64(12))

	// Interface type assertions
	var ta interface{} = "hello"

	s := ta.(string)
	fmt.Println(s)

	// myInt := ta.(int) //this would lead to panic, since ta ain't int, and no boolean is being read

	s, ok := ta.(string)
	fmt.Println(s, ok)

	f, ok := ta.(float64)
	fmt.Println(f, ok)

	p1 := Person{"James", 24}
	p2 := Person{"Taylor", 25}
	p3 := Person{"Charles", 34}
	// this will call their String() method automatically
	fmt.Println(p1, p2, p3)

	hosts := map[string]IPAddr{
		"loopback":   {127, 0, 0, 1},
		"google.com": {10, 25, 66, 12},
	}
	for name, ip := range hosts {
		fmt.Printf("%v: %v\n", name, ip)
	}

}

// Defining the type MyFloat according to Abser
type MyFloat float64

func (f MyFloat) Abs() float64 {
	if f < 0 {
		return float64(-f)
	}
	return float64(f)
}

// Defining the type Vertex according to Abser
type Vertex struct {
	X, Y float64
}

func (v *Vertex) Abs() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

// <nil> interfaces
type T struct {
	S string
}

func (t *T) M() {
	if t == nil {
		fmt.Println("<nil>")
		return
	}
	fmt.Println(t.S)
}

type F float64

func (f F) M() {
	fmt.Println(f)
}

// Stringer, an fmt interface which forces a behavior similar to java toString()
type Person struct {
	Name string
	Age  int
}

func (p Person) String() string {
	return fmt.Sprintf("%v, (%v years)", p.Name, p.Age)
}

// Stringer exercise
type IPAddr [4]byte

func (ipAddr IPAddr) String() string {
	var buffer bytes.Buffer
	for i := 0; i < len(ipAddr); i++ {
		buffer.WriteString(strconv.Itoa(int(ipAddr[i])))
		if i < len(ipAddr)-1 {
			buffer.WriteByte('.')
		}
	}
	return buffer.String()
}
