package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/mjibson/go-dsp/fft"
	"github.com/mjibson/go-dsp/spectral"
	"github.com/montanaflynn/stats"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"time"
)

type HealthResponse struct {
	Status    string `json:"status,omitempty"`
	Timestamp string `json:"timestamp,omitempty"`
}

type SplitComplex128 struct {
	Real float64 `json:"r,omitempty"`
	Imag float64 `json:"i,omniempty"`
}

type FFTRequest struct {
	SampleIndex int       `json:"sampleIndex,omitempty"`
	Sample      []float64 `json:"sample"`
}

type FFTNrgHzRequest struct {
	SampleRate  int       `json:"sampleRate,omitempty"`
	SampleIndex int       `json:"sampleIndex,omitempty"`
	Sample      []float64 `json:"sample"`
}

type FFTNrgHzMultiSampleRequest struct {
	SampleRate  int         `json:"sampleRate,omitempty"`
	SampleIndex int         `json:"sampleIndex,omitempty"`
	Samples     [][]float64 `json:"samples"`
}

type PWelchRequest struct {
	SampleRate  int       `json:"sampleRate,omitempty"`
	SampleIndex int       `json:"sampleIndex,omitempty"`
	Sample      []float64 `json:"sample"`
}

type FFTResponse struct {
	SampleIndex int               `json:"sampleIndex,omitempty"`
	SampleFFT   []SplitComplex128 `json:"sampleFFT"`
}

type FFTNrgResponse struct {
	SampleIndex int       `json:"sampleIndex,omitempty"`
	SampleNrg   []float64 `json:"nrg"`
}

type FFTNrgHzResponse struct {
	SampleIndex int       `json:"sampleIndex,omitempty"`
	SampleHz    []float64 `json:"hz"`
	SampleNrg   []float64 `json:"nrg"`
}

type PWelchResponse struct {
	SampleIndex int       `json:"sampleIndex,omitempty"`
	SamplePxx   []float64 `json:"pxx"`
	SampleFreq  []float64 `json:"freq"`
}

type App struct {
	Router *mux.Router
}

func (a *App) Initialize() {

	router := mux.NewRouter()
	router.HandleFunc("/fft/{id}", FFT).Methods("POST")
	router.HandleFunc("/fftNrg/{id}", FFTNrg).Methods("POST")
	router.HandleFunc("/fftHzNrg/{id}", FFTHzNrg).Methods("POST")
	router.HandleFunc("/fftHzNrgMultiSample/{id}", FFTHzNrgMultiSample).Methods("POST")
	router.HandleFunc("/pwelch/{id}", PWelch).Methods("POST")
	router.HandleFunc("/fft", HealthCheck).Methods("GET")
	a.Router = router
}

func (a *App) Run(addr string) {

	log.Fatal(http.ListenAndServe(addr, a.Router))
	log.SetFlags(0)
	log.SetOutput(ioutil.Discard)

}

// FFT
func FFT(w http.ResponseWriter, r *http.Request) {

	var fftRequest FFTRequest
	var fftResponse FFTResponse
	fftRequest = decodeFFTRequest(r)

	res := fft.FFTReal(fftRequest.Sample)

	fftResponse = buildFFTResponse(fftRequest, res)

	json.NewEncoder(w).Encode(fftResponse)
}

// FFT
func FFTNrg(w http.ResponseWriter, r *http.Request) {

	var fftRequest FFTRequest
	var fftNrgResponse FFTNrgResponse
	fftRequest = decodeFFTRequest(r)

	res := fft.FFTReal(fftRequest.Sample)

	fftNrgResponse = buildFFTNrgResponse(fftRequest, res)

	json.NewEncoder(w).Encode(fftNrgResponse)
}

// FFT Hz Nrg
func FFTHzNrg(w http.ResponseWriter, r *http.Request) {

	var fftNrgHzRequest FFTNrgHzRequest
	var fftNrgHzResponse FFTNrgHzResponse

	fftNrgHzRequest = decodeFFTNrgHzRequest(r)

	res := fft.FFTReal(fftNrgHzRequest.Sample)
	fftNrgHzResponse = buildFFTNrgHzResponse(fftNrgHzRequest, res)

	json.NewEncoder(w).Encode(fftNrgHzResponse)
}

// FFT Hz Nrg for multi samples
// Iterates through the request sample arrays, gets the FFT from each and
// Disregards last half of the DFT for assuming input real (sound)
// Operates with the half FFT results using percentile
func FFTHzNrgMultiSample(w http.ResponseWriter, r *http.Request) {

	var fftNrgHzMultiSampleRequest FFTNrgHzMultiSampleRequest
	var fftNrgHzResponse FFTNrgHzResponse

	fftNrgHzMultiSampleRequest = decodeFFTNrgHzMultiSampleRequest(r)
	var ffts [][]complex128 = make([][]complex128, len(fftNrgHzMultiSampleRequest.Samples))
	for i := 0; i < len(fftNrgHzMultiSampleRequest.Samples); i++ {

		res := fft.FFTReal(fftNrgHzMultiSampleRequest.Samples[i])
		// Allocating only the first half of the FFT result when assuming input real and continuous.
		ffts[i] = res[1 : len(res)/2+1]
	}

	// TODO calculate percentile of transposed matrix

	fftNrgHzResponse = buildFFTNrgHzResponseForMultiSample(fftNrgHzMultiSampleRequest, ffts)

	json.NewEncoder(w).Encode(fftNrgHzResponse)
}

func buildFFTNrgResponse(fftRequest FFTRequest, res []complex128) FFTNrgResponse {

	var fftNrgResponse FFTNrgResponse
	magSlice := make([]float64, len(res))

	for i := 0; i < len(res); i++ {
		value := res[i]
		real := real(value)
		imag := imag(value)
		mag := math.Sqrt(real*real + imag*imag)
		magSlice[i] = mag
	}

	fftNrgResponse = FFTNrgResponse{SampleIndex: fftRequest.SampleIndex, SampleNrg: magSlice}

	return fftNrgResponse
}

func decodeFFTRequest(r *http.Request) FFTRequest {

	var fftRequest FFTRequest

	//params := mux.Vars(r)
	//log.Print("params: ", params)
	_ = json.NewDecoder(r.Body).Decode(&fftRequest)
	//log.Print("request body: ", r.Body)
	//log.Print("request body decode: ", fftRequest)

	return fftRequest
}

func decodeFFTNrgHzRequest(r *http.Request) FFTNrgHzRequest {

	var fftNrgHzRequest FFTNrgHzRequest

	//params := mux.Vars(r)
	//log.Print("params: ", params)
	_ = json.NewDecoder(r.Body).Decode(&fftNrgHzRequest)
	//log.Print("request body: ", r.Body)
	//log.Print("request body decode: ", fftNrgHzRequest)

	return fftNrgHzRequest
}

func decodePWelchRequest(r *http.Request) PWelchRequest {
	var pwelchRequest PWelchRequest

	_ = json.NewDecoder(r.Body).Decode(&pwelchRequest)

	return pwelchRequest
}

func decodeFFTNrgHzMultiSampleRequest(r *http.Request) FFTNrgHzMultiSampleRequest {

	var fftNrgHzRequest FFTNrgHzMultiSampleRequest

	//params := mux.Vars(r)
	//log.Print("params: ", params)
	_ = json.NewDecoder(r.Body).Decode(&fftNrgHzRequest)
	//log.Print("request body: ", r.Body)
	//log.Print("request body decode: ", fftNrgHzRequest)

	return fftNrgHzRequest
}

func buildFFTResponse(fftRequest FFTRequest, res []complex128) FFTResponse {

	var fftResponse FFTResponse
	resSlice := make([]SplitComplex128, len(res))

	for i := 0; i < len(res); i++ {
		value := res[i]
		resSlice[i].Real = real(value)
		resSlice[i].Imag = imag(value)
	}

	fftResponse = FFTResponse{SampleIndex: fftRequest.SampleIndex, SampleFFT: resSlice}

	//log.Print("response body: ", fftResponse)
	return fftResponse
}

func buildFFTNrgHzResponse(fftNrgHzRequest FFTNrgHzRequest, res []complex128) FFTNrgHzResponse {

	var fftNrgHzResponse FFTNrgHzResponse

	magSlice := make([]float64, len(res))

	for i := 0; i < len(res); i++ {
		value := res[i]
		real := real(value)
		imag := imag(value)
		mag := math.Sqrt(real*real + imag*imag)
		magSlice[i] = mag
	}

	hzSlice := make([]float64, len(res))

	for i := 0; i < len(res); i++ {
		hzSlice[i] = float64(i * fftNrgHzRequest.SampleRate / len(res))
	}

	fftNrgHzResponse = FFTNrgHzResponse{SampleIndex: fftNrgHzRequest.SampleIndex, SampleHz: hzSlice, SampleNrg: magSlice}

	return fftNrgHzResponse
}

func buildFFTNrgHzResponseForMultiSample(fftNrgHzRequest FFTNrgHzMultiSampleRequest, res [][]complex128) FFTNrgHzResponse {

	var fftNrgHzResponse FFTNrgHzResponse
	var sampleLenght = len(res[0])

	nrgSlice := make([]float64, sampleLenght)

	//log.Println("sampleLenght:", sampleLenght)
	//log.Println("len(res):", len(res))

	for j := 0; j < sampleLenght; j++ {

		singleFreqSampleSlice := make([]float64, len(res))
		for i := 0; i < len(res); i++ {
			//log.Println("i: ", i, "j: ", j)
			value := res[i][j]
			//nrg := calcFftNrg(value)
			nrg := calcAndNormFftNrg(value, sampleLenght)
			singleFreqSampleSlice[i] = nrg
		}
		var err error = nil
		// nrgSlice[j], err = stats.Percentile(singleFreqSampleSlice, 15)
		nrgSlice[j], err = stats.PercentileNearestRank(singleFreqSampleSlice, 5)
		if err != nil {
			log.Println("err", err)
		} else {
			log.Println("Values:", singleFreqSampleSlice, "Percentile:", nrgSlice[j])
		}
		//log.Println("nrgSlice:", nrgSlice)
	}

	// Calculating the frequencies in hz
	hzSlice := make([]float64, sampleLenght)
	for i := 0; i < sampleLenght; i++ {
		hzSlice[i] = float64(i * fftNrgHzRequest.SampleRate / sampleLenght / 2)
	}

	fftNrgHzResponse = FFTNrgHzResponse{SampleIndex: fftNrgHzRequest.SampleIndex, SampleHz: hzSlice, SampleNrg: nrgSlice}

	return fftNrgHzResponse
}

// Calculates the real energy magnitued from a complex FFT result element.
// It does NOT normalize the result
func calcFftNrg(value complex128) float64 {
	real := real(value)
	imag := imag(value)
	nrg := math.Sqrt(real*real + imag*imag)
	return nrg

}

// Calculates the real energy magnitued from a complex FFT result element.
// It normalizes the result
func calcAndNormFftNrg(value complex128, sampleLenght int) float64 {
	real := real(value)
	imag := imag(value)
	nrg := math.Sqrt(real*real + imag*imag)
	return nrg / float64(sampleLenght)

}

// Health Check
func HealthCheck(w http.ResponseWriter, r *http.Request) {
	var response = HealthResponse{
		Status:    "OK",
		Timestamp: time.Now().String()}

	json.NewEncoder(w).Encode(response)

}

// PWelch
func PWelch(w http.ResponseWriter, r *http.Request) {

	var pwelchRequest PWelchRequest
	var pwelchResponse PWelchResponse
	var fs float64
	var opts *spectral.PwelchOptions

	pwelchRequest = decodePWelchRequest(r)

	log.Println("sampleRate", pwelchRequest.SampleRate)

	fs = float64(pwelchRequest.SampleRate)

	opts = &spectral.PwelchOptions{}
	opts.NFFT = len(pwelchRequest.Sample)

	pxx, freqs := spectral.Pwelch(pwelchRequest.Sample, fs, opts)

	pwelchResponse = buildPWelchResponse(pwelchRequest, pxx, freqs)

	json.NewEncoder(w).Encode(pwelchResponse)

}

func buildPWelchResponse(pwelchRequest PWelchRequest, pxx []float64,
	freqs []float64) PWelchResponse {

	var pwelchResponse PWelchResponse

	pwelchResponse = PWelchResponse{SampleIndex: pwelchRequest.SampleIndex,
		SampleFreq: freqs, SamplePxx: pxx}

	return pwelchResponse
}
