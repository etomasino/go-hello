package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

var a App
var fftHzNrgMultiSampleRequest []byte
var fftHzNrgMultiSampleRequestShort []byte
var fftHzNrgMultiSampleResponse []byte
var fftHzNrgMultiSampleResponseShort []byte

func TestMain(m *testing.M) {

	readJsonFiles()

	a = App{}
	a.Initialize()

	os.Exit(m.Run())
}

func TestHealthCheck(t *testing.T) {

	req, _ := http.NewRequest("GET", "/fft", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
	checkHealthCheckResponse(response, t)

}

func TestFftHzNrgMultiSample(t *testing.T) {

	req, _ := http.NewRequest("POST", "/fftHzNrgMultiSample/234", bytes.NewBuffer(fftHzNrgMultiSampleRequest))
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
	checkResponseBody(t, fftHzNrgMultiSampleResponse, response.Body.Bytes())

	reqShort, _ := http.NewRequest("POST", "/fftHzNrgMultiSample/234", bytes.NewBuffer(fftHzNrgMultiSampleRequestShort))
	responseShort := executeRequest(reqShort)
	checkResponseCode(t, http.StatusOK, responseShort.Code)
	checkResponseBody(t, fftHzNrgMultiSampleResponseShort, responseShort.Body.Bytes())

}

func readJsonFiles() {
	var err error
	fftHzNrgMultiSampleRequest, err = ioutil.ReadFile("json/fftHzNrgMultiSampleRequest.json")
	fftHzNrgMultiSampleRequestShort, err = ioutil.ReadFile("json/fftHzNrgMultiSampleRequestShort.json")
	fftHzNrgMultiSampleResponse, err = ioutil.ReadFile("json/fftHzNrgMultiSampleResponse.json")
	fftHzNrgMultiSampleResponseShort, err = ioutil.ReadFile("json/fftHzNrgMultiSampleResponseShort.json")

	if err != nil {
		panic(err)
	}
}

func checkHealthCheckResponse(response *httptest.ResponseRecorder, t *testing.T) {

	var m map[string]string
	json.Unmarshal(response.Body.Bytes(), &m)
	if m["status"] != "OK" {
		t.Errorf("Unexpected status code: %s", m["status"])
	}
}

func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}

func checkResponseBody(t *testing.T, expected, actual []byte) {

	if bytes.Compare(expected, actual) != 0 {
		t.Errorf("Respnses do NOT match. Expected response length: %d Actual response lenght: %d", len(expected), len(actual))
	}
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {

	rr := httptest.NewRecorder()
	a.Router.ServeHTTP(rr, req)

	return rr
}
