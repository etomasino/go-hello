package main

import "fmt"

const (
	// create a huge number shifting 1 bit left 100 places
	Big = 1 << 100

	// shifting it back again 99 places
	Small = Big >> 99

	// to the limit
	Limit = 1<<64 - 1
)

func needInt(x int) int { return x*10 + 1 }

func needFloat(x float64) float64 {
	return x * 0.1
}

func main() {
	//fmt.Println("Big: ", Big, " Small: ", Small)
	//fmt.Println(uint64(needInt(Limit)))
	fmt.Printf("Limit: %v\n", uint64(Limit))
	fmt.Println(needInt(Small))
	fmt.Println(needFloat(Small))
	fmt.Println(needFloat(Big))
}
