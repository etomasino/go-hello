package main

import (
	"fmt"
	"math"
	"strconv"
	"time"
)

type MyError struct {
	When time.Time
	What string
}

func (e *MyError) Error() string {
	return fmt.Sprintf("at %v, %s", e.When, e.What)
}

func run(i interface{}) (string, error) {
	strconv.Atoi("24")
	s, ok := i.(string)
	if ok {
		return s, nil
	} else {
		return "", &MyError{
			time.Now(),
			"No flipping shit was given",
		}
	}
}

func Sqrt(x float64) (float64, error) {
	if x < 0 {
		return 0, ErrNegativeSqrt(x)
	}

	var z float64 = x
	for i := 1; i < 10; i++ {
		z = z - (math.Pow(z, 2)-x)/(2*z)
	}
	return z, nil
}

func procError(i interface{}, err error) {
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(i)
	}
}

type ErrNegativeSqrt float64

func (e ErrNegativeSqrt) Error() string {
	// foced to conver to float64, since Sprintf would try to match to error
	// and then call e.Error() again, which would cause a infinite loop
	return fmt.Sprintf("Can't Sqrt negaive number: %v", float64(e))
}

func main() {
	//s, err := run("play")
	//s, err := run(124)

	procError(run("play"))
	procError(run(123))
	procError(Sqrt(4))
	procError(Sqrt(-3))

}
