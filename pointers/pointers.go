package main

import "fmt"

func playWithPointers() {
	i, j := 10, 20

	fmt.Println("j= ", j)

	p := &i
	fmt.Println(p)
	fmt.Println(*p)

	fmt.Println(*p + j)

	*p = 15
	fmt.Println(i)

	p = &j
	fmt.Println(*p + j)

	*p = *p / 2
	fmt.Println(*p)

	p2 := p

	p3 := p2

	p4 := p3
	*p4 = *p4 / 2
	fmt.Println("*p4= ", *p4, "j= ", j)

}

func main() {
	playWithPointers()
}
