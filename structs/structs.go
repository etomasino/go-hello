package main

import "fmt"

type Vertex struct {
	X int
	Y int
}

func main() {
	v := Vertex{1, 2}
	sp := &v.X
	*sp = 1234

	fsp := &v
	fsp.Y = 2345

	fmt.Println(v.X, v.Y)

	var v1 = Vertex{X: 1} //initializing only 1 element
	var v2 = Vertex{}
	var pv3 = &Vertex{1, 2}
	fmt.Println(v1, v2, *pv3)
}
