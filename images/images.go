package main

import (
	"fmt"
	"image"
	"image/color"

	"golang.org/x/tour/pic"
)

type SandImage struct {
	w, h int
}

func (si SandImage) ColorModel() color.Model {
	return color.RGBAModel
}

func (si SandImage) Bounds() image.Rectangle {
	var r = image.Rect(0, 0, si.w, si.h)
	return r
}

func (si SandImage) At(x, y int) color.Color {
	var c = color.RGBA{uint8(x), uint8(y), 255, 255}
	return c
}

func main() {

	m := image.NewRGBA(image.Rect(0, 0, 100, 100))
	fmt.Println(m.Bounds())
	fmt.Println(m.At(0, 0).RGBA())

	var si = SandImage{
		100,
		100,
	}

	pic.ShowImage(si)

}
