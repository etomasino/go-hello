package main

import (
	"fmt"
	"io"
	"os"
	"strings"

	"golang.org/x/tour/reader"
)

type MyInfiniteReader struct{}

func (r MyInfiniteReader) Read(b []byte) (int, error) {
	for i := 0; i < len(b); i++ {
		b[i] = 'A'
	}
	return len(b), nil
}

// Reader wrapping a reader
type rot13Reader struct {
	r io.Reader
}

func (r rot13Reader) Read(b []byte) (int, error) {

	// b2 := make([]byte, 8)
	readBytes := 0
	for {
		n, err := r.r.Read(b)
		readBytes += n

		// doing some rot13
		for i := 0; i < n; i++ {
			if 65 <= b[i] && b[i] <= 77 || 97 <= b[i] && b[i] <= 109 {
				b[i] += 13
			} else if 78 <= b[i] && b[i] <= 90 || 110 <= b[i] && b[i] <= 122 {
				b[i] -= 13
			}
		}

		//fmt.Printf("n=%v, err=%v, b=%v\n", n, err, b)
		if n == 0 || err != nil {
			break
		}
	}
	// important to return io.EOF to avoid endless loops
	return readBytes, io.EOF
}

func main() {

	r := strings.NewReader("Hello Reader!")

	b := make([]byte, 8)

	for {
		n, err := r.Read(b)
		fmt.Printf("n=%v, err=%v, b=%v\n", n, err, b)
		fmt.Printf("b[:n]=%q\n", b[:n])
		if err == io.EOF {
			break
		}
	}

	reader.Validate(MyInfiniteReader{})

	// Playing with rot13 reader
	s := strings.NewReader("Lbh penpxrq gur pbqr!")
	r13 := rot13Reader{s}
	io.Copy(os.Stdout, &r13)

}
