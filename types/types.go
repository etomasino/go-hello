package main

import (
	"fmt"
	"math"
	"math/cmplx"
)

var (
	ToBe     bool       = false
	MaxInt   int        = 1<<32 - 1
	Max64Int uint64     = 1<<64 - 1
	z        complex128 = cmplx.Sqrt(-5 + 12i)
)

func MessWithTypes() {
	var x, y int = 3, 4
	var f float64 = math.Sqrt(float64(x*x + y*y))
	fmt.Println("Messing up: ", f)
}

func PrintMyType() {
	v := 42 + 3i
	fmt.Printf("My type is %T\n", v)
}

func main() {

	fmt.Println("Max int 64: ", Max64Int)
	fmt.Println("Complex: ", z)
	MessWithTypes()
	PrintMyType()

}
