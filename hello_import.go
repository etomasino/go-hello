package main

import "fmt"
import "bitbucket.org/etomasino/go-hello/stringutil"

func notMain() {

	fmt.Println("Hello\n")
	fmt.Printf(stringutil.Reverse("hello world"))

}
